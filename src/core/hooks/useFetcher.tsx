import { useEffect, useState } from "react";


export function useFetcher<T, P>(
  fetcher: (param: P) => Promise<T>,
  initialParam: P
) {
  const [data, setData] = useState<T | null>(null);
  const [params, setParams] = useState<P>(initialParam)
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setMessage("");
    fetcher(params)
      .then((results) => setData(results))
      .catch((err) => setMessage(err.message))
      .finally(() => setLoading(false));
  }, [params]);

  return [{ params, data, loading, message }, setParams] as const;
}

export default useFetcher