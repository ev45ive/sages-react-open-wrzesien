import axios from "axios"
import { User } from "../model/User"


export const fetchCurrentUserProfile = () => {
    return axios.get<User>('https://api.spotify.com/v1/me')
        .then(resp => resp.data)
}