import axios from "axios";
import { Playlist } from "../model/Playlist";
import { Album, AlbumsSearchResponse, PagingObject } from "../model/Search";
import { User } from "../model/User";


export const fetchSearchResults = (query: string, type = 'album') => {

    return axios.get<AlbumsSearchResponse>('https://api.spotify.com/v1/search', {
        // headers: {
        //     Authorization: 'Bearer tajnytokenplacki'
        // },
        params: {
            q: query,
            type: type
        }
    })
        .then(res => {
            return res.data.albums.items
        })
        .catch(err => {
            return Promise.reject(new Error(err.response.data.error.message))
        })
}

export const fetchAlbumById = (id: Album['id']) => {
    return axios.get<Album>(`https://api.spotify.com/v1/albums/${id}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            return Promise.reject(new Error(err.response.data.error.message))
        })
}

export const fetchPlaylistByUserId = (user_id: User['id']) => {
    return axios.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/${user_id}/playlists`).then(res => res.data)
}

// export const fetchSearchResults = (query: string, type = 'album') => {

//     return axios.get('/albums.json')
//         .then(res => {
//             return res.data
//         })

// }