import axios from "axios";
import { Dispatch, Reducer } from "redux";
import { AppState } from "../../store";
import { Playlist } from "../model/Playlist";
import { PagingObject } from "../model/Search";
import { fetchPlaylistByUserId } from "../services/search";

export interface PlaylistsState {
  items: Playlist[];
  loading: boolean
  selectedId?: Playlist["id"];
  mode: "details" | "create" | "edit";
}
export const initialState: PlaylistsState = {
  items: [],
  loading: false,
  selectedId: undefined,
  mode: "details",
};
interface PLAYLISTS_LOAD_START {
  type: "PLAYLISTS_LOAD_START";
}
interface PLAYLISTS_LOAD_SUCCESS {
  type: "PLAYLISTS_LOAD_SUCCESS";
  payload: { data: Playlist[]; };
}
interface PLAYLISTS_LOAD_FAILED {
  type: "PLAYLISTS_LOAD_FAILED";
  payload: { error: Error; };
}interface PLAYLISTS_SELECT {
  type: "PLAYLISTS_SELECT";
  payload: { id: Playlist["id"]; };
}
interface PLAYLISTS_REMOVE {
  type: "PLAYLISTS_REMOVE";
  payload: { id: Playlist["id"]; };
}
interface PLAYLISTS_CHANGE_MODE {
  type: "PLAYLISTS_CHANGE_MODE";
  payload: { mode: PlaylistsState["mode"]; };
}
interface PLAYLISTS_UPDATE {
  type: "PLAYLISTS_UPDATE";
  payload: { data: Playlist; };
}
interface PLAYLISTS_CREATE {
  type: "PLAYLISTS_CREATE";
  payload: { data: Playlist; };
}
type Actions =
  | PLAYLISTS_LOAD_START
  | PLAYLISTS_LOAD_SUCCESS
  | PLAYLISTS_LOAD_FAILED
  | PLAYLISTS_SELECT
  | PLAYLISTS_REMOVE
  | PLAYLISTS_CHANGE_MODE
  | PLAYLISTS_UPDATE
  | PLAYLISTS_CREATE;

export const reducer: Reducer<PlaylistsState, Actions> = (state = initialState, action) => {

  switch (action.type) {
    case "PLAYLISTS_LOAD_START":
      return { ...state, loading: true };
    case "PLAYLISTS_LOAD_SUCCESS":
      return { ...state, items: action.payload.data, loading: false };
    case "PLAYLISTS_LOAD_FAILED":
      return { ...state, loading: false, error: action.payload.error };
    case "PLAYLISTS_SELECT":
      return { ...state, selectedId: action.payload.id };
    case "PLAYLISTS_CHANGE_MODE":
      return { ...state, mode: action.payload.mode };
    case "PLAYLISTS_REMOVE":
      return {
        ...state,
        items: state.items.filter((p) => p.id !== action.payload.id),
      };
    case "PLAYLISTS_UPDATE":
      return {
        ...state,
        items: state.items.map((p) => p.id === action.payload.data.id ? action.payload.data : p
        ),
        mode: "details",
      };
    case "PLAYLISTS_CREATE":
      return {
        ...state,
        items: [...state.items, action.payload.data],
        mode: "details",
      };
    default:
      return state;
  }
};
export const playlistsLoadStart = (): PLAYLISTS_LOAD_START => ({
  type: "PLAYLISTS_LOAD_START" as const,
});
export const playlistsLoadSuccess = (data: Playlist[]): PLAYLISTS_LOAD_SUCCESS => ({
  type: "PLAYLISTS_LOAD_SUCCESS" as const,
  payload: { data },
});
export const playlistsLoadFailed = (error: Error): PLAYLISTS_LOAD_FAILED => ({
  type: "PLAYLISTS_LOAD_FAILED" as const,
  payload: { error },
});
export const playlistsSelect = (id: Playlist["id"]): PLAYLISTS_SELECT => ({
  type: "PLAYLISTS_SELECT" as const,
  payload: { id },
});
export const playlistsRemove = (id: Playlist["id"]): PLAYLISTS_REMOVE => ({
  type: "PLAYLISTS_REMOVE" as const,
  payload: { id },
});
export const playlistsChangeMode = (
  mode: PlaylistsState["mode"]
): PLAYLISTS_CHANGE_MODE => ({
  type: "PLAYLISTS_CHANGE_MODE" as const,
  payload: { mode },
});
export const playlistUpdate = (data: Playlist): PLAYLISTS_UPDATE => ({
  type: "PLAYLISTS_UPDATE" as const,
  payload: { data },
});
export const playlistCreate = (data: Playlist): PLAYLISTS_CREATE => ({
  type: "PLAYLISTS_CREATE" as const,
  payload: { data },
});

// === SELECTORS : ===
// https://github.com/reduxjs/reselect

export const playlistFeatureSelector = (state: AppState) => state.playlists

export const selectedPlaylistSelector = (state: AppState) => {
  const feature = playlistFeatureSelector(state)
  return feature.items.find((p) => p.id === feature.selectedId);
}

export const playlistsSelector = (state: AppState) => {
  const feature = playlistFeatureSelector(state)
  return feature.items
}


export const actions = {
  load: playlistsLoadSuccess,
  select: playlistsSelect,
  remove: playlistsRemove,
  changemode: playlistsChangeMode,
  update: playlistUpdate,
  create: playlistCreate,
}


// ==== async action creators ===

export const fetchPlaylists = (user_id = 'me') => (dispatch: Dispatch) => {

  dispatch(playlistsLoadStart())

  fetchPlaylistByUserId(user_id)
    .then(data => dispatch(playlistsLoadSuccess(data.items)))
    .catch((error) => dispatch(playlistsLoadFailed(error)))

  // return {type:'FETCH_PLAYLSITS'}
}