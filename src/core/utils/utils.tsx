
// https://www.npmjs.com/package/classnames

/*
    <div className={cls("list-group-item list-group-item-action", playlist.id === selectedId && "active" )}>
        {index + 1}. {playlist.name}
    </div>    
*/
// export const cls = (...classes: (string | boolean)[]) => classes.filter(c => c !== false).join(' ');
export const cls = (...classes: (string | boolean)[]) => classes.filter(Boolean).join(' ');
