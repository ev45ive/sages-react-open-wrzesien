import { NavLink } from "react-router-dom";
import { UserWidget } from "./UserWidget";

interface Props {}

export const NavBar = (props: Props) => {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
      <div className="container">
        <a className="navbar-brand" href="#">
          MusicApp
        </a>
        <button className="navbar-toggler" type="button">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink
                className="nav-link"
                aria-current="page" exact activeClassName=" placki active "
                to="/search"
              >
                Search
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/playlists">
                Playlists
              </NavLink>
            </li>
          </ul>

          <div className="ms-auto navbar-text">
            {/* <Route path="/app/" component={UserWidget} /> */}
            <UserWidget />
          </div>

          {/* <div className="ms-auto navbar-text">
            <UserProfileContext.Consumer>
              {({ login, logout, user }) => (
                <UserWidget user={user} login={login} logout={logout} />
              )}
            </UserProfileContext.Consumer> 
          </div>*/}

          {/* <div className="ms-auto navbar-text">
            <UserProfileContext.Consumer>
              {(props) => (
                <UserWidget  {...props} />
              )}
            </UserProfileContext.Consumer> 
          </div> */}
        </div>
      </div>
    </nav>
  );
};
