interface Track {
  duration_ms: number;
  id: string;
  name: string;
  type: 'track';
}

interface Show {
  type: 'show';
  id: string;
  name: string;
  duration_ms: number;
}
export interface Playlist {
  id: string;
  name: string;
  public: boolean;
  description: string;
  tracks?: Track[]
  type: 'playlist'
}

// typeof x == 'string'
// x.costam == 'tagA'
// x instanceof PersonCtr // new (Class / Fn)
// 'costam' in x

// https://github.com/gvergnaud/ts-pattern
// https://github.com/colinhacks/zod#unions

function exhaustivenessCheck(never: never): never {
  throw new Error('Unexepcted result')
}

function showResultByTag(result: Playlist | Track) {
  switch (result.type) {
    case 'playlist': return `${result.name} ( ${result.tracks?.length || 'no'} tracks )`
    case 'track': return `${result.name} ( ${result.duration_ms}ms )`
    default:
      exhaustivenessCheck(result)
    // const _never: never = result
    // throw new Error('Unexepcted result')
  }
}



function showResultByShape(result: Playlist | Track | Show) {
  if ('public' in result) {
    return `${result.name} ( ${result.tracks?.length || 'no'} tracks )`
  } else if ('duration_ms' in result && result.type === 'show') {
    return `${result.name} ( ${result.duration_ms}ms )`
  } else if (result.type === 'track') {
    return `${result.name} ( ${result.duration_ms}ms )`
  } else {
    exhaustivenessCheck(result)
  }
}


function showPlaylist(result: Playlist) {

  // if (result.tracks) let tracks = result.tracks.length;
  // let tracks = result.tracks && result.tracks.length || 0
  // let tracks = result.tracks? result.tracks.length : 0
  // let tracks = (result.tracks ?? []).length
  let tracks = result.tracks?.length || 0

  // let tracks = (result.tracks as Track[]).length || 0 // Type assersion
  // let tracks = result.tracks!.length || 0 // Not-Null assersion

  if (!result.tracks) return `${result.name} - no tracks`

  // MY playlist - (20 tracks)
  return `${result.name} - (${tracks} tracks)`
}







// type Id = string | number;

function getItem(someID: string | number) {
  // someID as string // unsafe
  // someID.toString() // safe

  if (typeof someID === 'string') { // type guard
    return someID.toLocaleUpperCase()
  } else {
    return someID.toExponential(2)
  }
}
