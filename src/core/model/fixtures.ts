import { Playlist } from "./Playlist";
import { SimpleAlbum } from "./Search";

export const playlistsMock: Playlist[] = [
  {
    id: "123",
    name: "React Hits",
    public: true,
    description: "Best React Hits  ",
  },
  {
    id: "234",
    name: "React Top20",
    public: false,
    description: "Best React 20",
  },
  {
    id: "345",
    name: "Best of React",
    public: true,
    description: "Best of React Hits",
  },
];

export const mockAlbums: SimpleAlbum[] = [
  {
    id: "123",
    name: "Album 123",
    images: [
      { url: "https://www.placecage.com/c/300/300", width: 300, height: 300 },
    ],
  },
  {
    id: "234",
    name: "Album 234",
    images: [
      { url: "https://www.placecage.com/c/200/200", width: 300, height: 300 },
    ],
  },
  {
    id: "345",
    name: "Album 345",
    images: [
      { url: "https://www.placecage.com/c/300/300", width: 300, height: 300 },
    ],
  },
  {
    id: "456",
    name: "Album 456",
    images: [
      { url: "https://www.placecage.com/c/400/400", width: 300, height: 300 },
    ],
  },
];
