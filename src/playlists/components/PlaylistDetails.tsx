// Type Script React Arrow Function Componen
// tsrafce

import React from "react";
import { Playlist } from "../../core/model/Playlist";
import "./PlaylistsDetails.css";

interface Props {
  //   playlist: Playlist | undefined;
  playlist?: Playlist;
  onEdit?: () => void;
}

export const PlaylistDetails = ({ playlist, onEdit }: Props) => {
  if (!playlist) {
    return <p className="alert alert-info">Please select playlist</p>;
  }

  return (
    <div>
      <p>PlaylistDetails</p>

      <dl title={playlist.name} data-playlist-id={playlist.id}>
        <dt>Name:</dt>
        <dd data-testid="playlist-name">{playlist.name}</dd>

        <dt>Public:</dt>
        <dd
          className={playlist.public ? "playlist-public" : "playlist-private"}
        >
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description.slice(0, 20)}</dd>
      </dl>
      {onEdit && <input
        type="button"
        value="Edit"
        className="btn btn-info"
        onClick={onEdit}
      />}
    </div>
  );
};

// PlaylistDetails.propTypes = {}