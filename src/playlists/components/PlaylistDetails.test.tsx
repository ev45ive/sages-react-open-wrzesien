import { Playlist } from "../../core/model/Playlist"
import { render, screen, logRoles } from '@testing-library/react';
import { PlaylistDetails } from "./PlaylistDetails";
import { playlistsMock } from "../../core/model/fixtures";


describe.only('PlaylistDetails', () => {

  const setup = ({ mock = playlistsMock[0] }) => {
    const editSpy = jest.fn()
    const { rerender, container } = render(<PlaylistDetails onEdit={editSpy} playlist={mock} />)

    return { rerender, container, editSpy }
  }


  it('renders playlist', async () => {
    const { container, editSpy } = setup({ mock: playlistsMock[0] })

    // logRoles(container)

    // await screen.findByRole('button', { name: 'Edit' }) // wait for button to appear
    screen.getByRole('button', { name: 'Edit' }).click() // require button and click
    expect(editSpy).toHaveBeenCalled()

    expect(screen.getByTestId('playlist-name')).toHaveTextContent(playlistsMock[0].name)
  })


})