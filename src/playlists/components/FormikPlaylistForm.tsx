import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FormikProps,
  useField,
} from "formik";
import * as Yup from "yup";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => void;
}

interface FormValues {
  name: string;
  public: boolean;
  description: string;
}

export const FormikPlaylistForm = ({ onCancel, onSave, playlist }: Props) => {
  return (
    <div>
      <Formik
        initialValues={playlist}
        enableReinitialize={true}
        validationSchema={Yup.object({
          name: Yup.string()
            .min(5, "Must be 5 characters or more")
            .required("Required"),
          public: Yup.boolean().required("Required"),
          description: Yup.string(), //.required("Required"),
        })}
        onSubmit={(values, { setSubmitting }) => {
          onSave(values);
        }}
      >
        {({ values, errors }: FormikProps<Playlist>) => (
          <Form>
            <MyTextInput label="Name:" name="name" />
            {/* <div className="form-group mb-3">
              <label htmlFor="name">Name:</label>
              <Field
                name="name"
                type="text"
                className="form-control"
                placeholder="Name"
              />
              <span>{values.name.length} / 170</span>
              <ErrorMessage name="name" />
            </div> */}

            <div className="form-group mb-3">
              <label htmlFor="public">
                <Field name="public" type="checkbox" /> Public
              </label>
              <ErrorMessage name="public" />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="description">Description:</label>
              <Field
                name="description"
                as="textarea"
                className="form-control"
              />
              <ErrorMessage name="description" />
            </div>

            <button type="button" className="btn btn-danger" onClick={onCancel}>
              Cancel
            </button>
            <button type="submit" className="btn btn-success">
              Submit
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export const MyTextInput = ({ label, ...props }: any) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input>. We can use field meta to show an error
  // message if the field is invalid and it has been touched (i.e. visited)
  const [field, meta] = useField(props);
  return (
    <div className="form-group">
      <label htmlFor={props.id || props.name}>{label}</label>
      <input className="form-control" {...field} {...props} />

      <span className="float-end">{field.value.length} / 170</span>

      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </div>
  );
};
