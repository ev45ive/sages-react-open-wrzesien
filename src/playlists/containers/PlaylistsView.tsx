import { useEffect, useState } from "react";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistsList } from "../components/PlaylistsList";
import { Playlist } from "../../core/model/Playlist";
import { playlistsMock } from "../../core/model/fixtures";
import { FormikPlaylistForm } from "../components/FormikPlaylistForm";

export interface Props {}

export const PlaylistsView = (props: Props) => {
  const [selectedId, setSelectedId] = useState<string | undefined>("123");
  const [selectedPlaylist, setSelectedPlaylist] = useState<
    Playlist | undefined
  >();

  const [playlists, setPlaylists] = useState<Playlist[]>([]);

  const [mode, setMode] = useState<"edit" | "details" | "create">("details");

  useEffect(() => {
    setTimeout(() => setPlaylists(playlistsMock), 500);
  }, []);

  useEffect(() => {
    setSelectedPlaylist(playlists.find((p) => p.id === selectedId));
  }, [selectedId, playlists]);

  const selectPlaylistById = (id: Playlist["id"]) => {
    setSelectedId(id === selectedId ? undefined : id);
  };

  const createMode = () => {
    setMode("create");
  };
  const editMode = () => setMode("edit");
  const cancelEdit = () => setMode("details");

  const savePlaylist = (draft: Playlist) => {
    setPlaylists(playlists.map((p) => (p.id === draft.id ? draft : p)));
    setMode("details"); // render!
  };

  const removePlaylist = (id: Playlist["id"]) => {
    setPlaylists(playlists.filter((p) => p.id !== id));
  };

  const createPlaylist = (draft: Playlist) => {
    draft.id = (Date.now() + Math.ceil(Math.random() * 100_000)).toString();
    // playlists.push(draft); // Mutate array - won`t rerender!
    setPlaylists([...playlists, draft]);
    setSelectedId(draft.id);
    setMode("details");
  };

  return (
    <div>
      {/* .row>.col*2 */}
      <div className="row">
        <div className="col">
          <PlaylistsList
            playlists={playlists}
            selectedId={selectedId}
            onRemove={removePlaylist}
            onSelect={selectPlaylistById}
          />
          <button className="btn btn-info float-end mt-3" onClick={createMode}>
            Create new playlist
          </button>
        </div>
        <div className="col">
          {mode === "details" ? (
            <div>
              <PlaylistDetails playlist={selectedPlaylist} onEdit={editMode} />
            </div>
          ) : null}

          {selectedPlaylist && mode === "edit" && (
            <div>
              {/* <PlaylistForm
                playlist={selectedPlaylist}
                onCancel={cancelEdit}
                onSave={savePlaylist}
              /> */}
              <FormikPlaylistForm
                playlist={selectedPlaylist}
                onCancel={cancelEdit}
                onSave={savePlaylist}
              />
            </div>
          )}

          {selectedPlaylist && mode === "create" && (
            <div>
              <PlaylistForm
                playlist={{
                  id: "",
                  description: "",
                  name: "",
                  public: false,
                }}
                onCancel={cancelEdit}
                onSave={createPlaylist}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
