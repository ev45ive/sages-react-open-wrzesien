import React from "react";
import { Component } from "react";

interface Props {
}
interface State {
  deserUlubiony: string;
}

export default class PlaylistsViewLifecycle extends Component<Props,State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      deserUlubiony:'placki'
    };
  }

  // static getDerivedStateFromProps(props:Props){ ...
  componentDidMount() {
    // getUser
    // updatePlaylist
  }

  static getDerivedStateFromProps(props: Props) {
    // this // undefined
    // getDataToUpdatePlaylist
    return {
      /* new state */
    };
  }

  componentDidUpdate() {
    // if(props){ this.setState(..props)} // Trigger rerender after render
    // getUser
    // updatePlaylist
  }

  componentWillUnmount() {
    // forgetUser
    // removePlaylist
  }

  render() {
    // this.setState({}) // Too many re-renders. React limits the number of renders to prevent an infinite loop
    return (
      <div
        onClick={() => {
          // if(this.state.deserUlubiony !== 'ciastka')
          this.setState({ /* ...this.state, */ deserUlubiony : 'ciastka '});
        }}
      >
        {this.state.deserUlubiony}
      </div>
    );
  }
}
