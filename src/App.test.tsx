import React from 'react';
import { render, screen } from '@testing-library/react';

jest.mock("./core/components/NavBar")
import { NavBar } from "./core/components/NavBar";
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/MusicApp/i);
  expect(linkElement).toBeInTheDocument();
});
