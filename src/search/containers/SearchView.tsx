import { useEffect } from "react";
import { RouteComponentProps, useHistory, useLocation } from "react-router-dom";
import { fetchSearchResults } from "../../core/services/search";
import { SearchForm } from "../components/SearchForm";
import { SearchResults } from "../components/SearchResults";
import { Loading } from "./AlbumDetailsView";
import { useFetcher } from "../../core/hooks/useFetcher";

interface Props extends RouteComponentProps {}

export const SearchView = (props: Props) => {
  // const q = new URLSearchParams(props.location.search).get("q");
  // props.history.push()

  const history = useHistory();
  const location = useLocation();
  const q = new URLSearchParams(location.search).get("q");

  const [{ data, loading, message, params: query }, setQuery] = useFetcher(
    (query) => fetchSearchResults(query),
    q || "batman"
  );

  // Watch param change:
  useEffect(() => {
    q && setQuery(q);
  }, [q]);

  // const searchAlbums = (query: string) => history.push("/search?q=" + query);
  const searchAlbums = (query: string) => history.replace("/search?q=" + query);

  if (!data) return <Loading />;

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm query={query} onSearch={searchAlbums} />
        </div>
      </div>

      <div className="row">
        <div className="col">
          {message && <p className="alert alert-danger">{message}</p>}

          <SearchResults results={data} />
        </div>
      </div>
    </div>
  );
};
