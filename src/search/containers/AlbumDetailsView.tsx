import { useEffect, useRef, useState } from "react";
import { AlbumCard } from "../components/AlbumCard";
import { Album, Track } from "../../core/model/Search";
import { useParams } from "react-router";
import { fetchAlbumById } from "../../core/services/search";
import { TrackList } from "./TrackList";
import { useFetcher } from "../../core/hooks/useFetcher";

interface Props {}

export const AlbumDetailsView = (props: Props) => {
  const { album_id } = useParams<{ album_id: Album["id"] }>();
  const [currentTrack, setCurrentTrack] = useState<Track | null>(null);
  const audioRef = useRef<HTMLAudioElement>(null);

  // const [album, setAlbum] = useState<Album | null>(null);
  // const [message, setMessage] = useState("");
  // const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   setLoading(true);
  //   setMessage("");
  //   fetchAlbumById(album_id)
  //     .then((album) => setAlbum(album))
  //     .catch((err) => setMessage(err.message))
  //     .finally(() => setLoading(false));
  // }, [album_id]);

  const [{ data: album, loading, message, params }, setAlbumId] = useFetcher(
    (id) => fetchAlbumById(id),
    album_id
  );

  useEffect(() => {
    setAlbumId(album_id);
  }, [album_id]);

  const playTrack = (track: Track): void => {
    setCurrentTrack(track);
  };

  useEffect(() => {
    if (!audioRef.current) return;
    audioRef.current.volume = 0.2;
    audioRef.current.play();
  }, [currentTrack]);

  if (message) return <p className="alert alert-danger">{message}</p>;

  if (!album) return <Loading />;

  return (
    <div>
      <div className="row">
        <div className="col">
          <h1>{album.name}</h1>
          <small className="text-muted">{album_id}</small>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album} />
        </div>
        <div className="col">
          <dl>
            <dt>Artist</dt>
            <dd>{album.artists[0].name}</dd>
          </dl>

          <audio
            ref={audioRef}
            src={currentTrack?.preview_url}
            controls
            className="w-100"
          ></audio>

          <h4>Tracks</h4>
          <TrackList tracks={album.tracks.items} onPlay={playTrack} />
        </div>
      </div>
    </div>
  );
};

export const Loading = () => (
  <div className="d-flex justify-content-center">
    <div className="spinner-border" role="status">
      <span className="visually-hidden">Loading...</span>
    </div>
  </div>
);
