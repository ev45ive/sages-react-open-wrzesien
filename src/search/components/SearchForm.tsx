import { useEffect, useRef, useState } from "react";
interface Props {
  onSearch(query: string): void;
  query: string;
}

export const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
  const [query, setQuery] = useState(parentQuery);
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => inputRef.current?.focus(), []);

  useEffect(() => {
    setQuery(parentQuery);
  }, [parentQuery]);

  useEffect(() => {
    if (query === parentQuery) {
      return;
    }
    const handle = setTimeout(() => {
      onSearch(query);
    }, 500);

    return () => clearTimeout(handle);
  }, [query]);

  const submit = () => {
    onSearch(query);
  };

  return (
    <div>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={query}
          ref={inputRef}
          onChange={(e) => setQuery(e.currentTarget.value)}
          //   onChange={(e) => onSearch(e.currentTarget.value)}
        />

        <button
          className="btn btn-outline-secondary"
          type="button"
          onClick={submit}
        >
          Search
        </button>
      </div>
    </div>
  );
};
