
## GIT
cd ..
git clone https://bitbucket.org/ev45ive/sages-react-open-wrzesien.git
cd sages-react-open-wrzesień
npm install
npm start

## GIT Update
git stash -u
git pull

or:
git pull -u origin master

## Instalacje

node -v 
v14.17.0

git --version
git version 2.31.1.windows.1

npm -v 
6.14.6

chrome://version
Google Chrome 93.0.4577.63 

## React Create App
npx create-react-app --help

npx create-react-app . --template typescript --use-npm 

npm start

## bootstrap
npm install bootstrap

## Extenstions 
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

## Playlsits

mkdir -p src/playlists/containers
touch src/playlists/containers/PlaylistsView.tsx

mkdir -p src/playlists/components
touch src/playlists/components/PlaylistsList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistForm.tsx

## Search 

mkdir -p src/core/model
mkdir -p src/core/utils


mkdir -p src/search/containers
touch src/search/containers/SearchView.tsx

mkdir -p src/search/components
touch src/search/components/SearchForm.tsx
touch src/search/components/SearchResults.tsx
touch src/search/components/AlbumCard.tsx


## AXios + OAuth2

npm i react-oauth2-hook immutable prop-types react-storage-hook