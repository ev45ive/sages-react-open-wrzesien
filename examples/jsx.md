```tsx

interface User {
  id: string;
  name: string;
  color: string;
  pet: string;
}

const users: any[] = [
  {
    id: "123",
    name: "Bob",
    color: "blue",
    pet: "Fish",
  },
  {
    id: "234",
    name: "Alice",
    color: "red",
    pet: "cat",
  },
];

// const Person = (props: any) =>
const Person = (props: { person: User }) => (
  <li id={"user_" + props.person.id} className="placki" style={ { color: props.person.color } }>
    <p>{props.person.name} and {props.person.pet}</p>
  </li>
);
// React.createElement(
//   "li",
//   {
//     id: "123",
//     className: "placki",
//     style: { color: props.person.color },
//   },
//   React.createElement("p", {}, `${props.person.name} and ${props.person.pet}`)
//   // React.createElement("input", {})
// );

// const persons = users.map(user => Person({person:user}) );
const persons = users.map((user, index) =>
  React.createElement(Person, { key: user.id, person: user })
);

// const list = React.createElement(
//   "div",
//   {},
//   React.createElement("ul", {}, persons)
//   // React.createElement("input", {})
// );

// Function as a Component
// Person({ key: user.id, person: user })
// React.createElement(Person, { key: user.id, person: user })
// <Person person={user} key={user.id} />

const List = () => (
  <div>
    {/* <ul> { persons } </ul> */}
    <ul>
      {users.map((user, index) => (
        <Person person={user} key={user.id} />
      ))}
    </ul>
    <input type="text" />
  </div>
);

ReactDOM.render(<List />, document.getElementById("root"));

```