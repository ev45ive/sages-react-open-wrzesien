"use strict";
// @ts-check
Object.defineProperty(exports, "__esModule", { value: true });
exports.sub = void 0;
/**
 * Substracts 2 numbers
 * @param {number} a Substractor
 * @param {number} b Sustractee
 * @returns number
 */
function sub(a, b) {
    return a - b;
}
exports.sub = sub;
// @ts-expect-error
sub(2, 'x');
