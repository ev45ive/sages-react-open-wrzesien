/**
 * Substracts 2 numbers
 * @param {number} a Substractor
 * @param {number} b Sustractee
 * @returns number
 */
export function sub(a: number, b: number): number;
