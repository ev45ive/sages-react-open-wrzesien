// @ts-check

/**
 * Substracts 2 numbers
 * @param {number} a Substractor
 * @param {number} b Sustractee
 * @returns number
 */
export function sub(a, b) {
  return a - b
}

// @ts-expect-error
sub(2, 'x')