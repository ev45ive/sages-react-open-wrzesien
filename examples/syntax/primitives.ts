(function () {
  {
    var isThisBookOkay = true;
    isThisBookOkay = true
    isThisBookOkay = false
    // isThisBookOkay = 1
  }
  // isThisBookOkay
})()
// isThisBookOkay

const myNum1: number = 12
const myNum2 = 12

const firstName: string = "Mateusz";
const lastName: string = "Kulesza\
";
const hello: string = `Hello,
${firstName} ${lastName}!`;
// Hello,\nMateusz Kulesza!

declare function getValue(key: string): any

// OK, return value of 'getValue' is not checked
const str: string = getValue("myString")
const num: number = getValue("myString")

// Any propagates down to members:
let looselyTyped: any = {};
let result = looselyTyped.a.b.c.d.getMeAMIllionDollars() + 2
// ^ = let d: any


function warnUser(): void {
  console.log("This is my warning message");
  // return 123
}
function test2(callback: () => void) { }
test2(warnUser)

// const y: void = null // Type 'null' is not assignable to type 'void'
const z: void = undefined
const x: void = warnUser()


function userTextInput(maybe: string | null | undefined) {
  if (maybe)
    maybe.toLocaleUpperCase() // Object is possibly 'null' or 'undefined'.
}


// Function returning never must not have a reachable end point
function error(message: string): never {
  throw new Error(message);
  // const x = 12; // Unreachable code detected.
}

// Inferred return type is never
function fail() {
  error("Something failed");
  const x = 12;
}

// Function returning never must not have a reachable end point
function infiniteLoop(): never { while (true) { } }

// const obj: {
//   items: string[];
// } = { items: [] as string[] }

const obj = { items: [] }

// let obj2:object = null  // Type 'null' is not assignable to type 'object'
// typeof null === 'object' // true


// Way 1: Type[]
let list1: number[] = [1, 2, 3,];
// let list1: number[] = [1, 2, 3 ,'x']; // Error
// The second way uses a generic array type, Array<elemType>:

// Way 2: Array<Type>
let list2: Array<number> = [1, 2, 3];

// This is NOT and Array! This is a tuple!
let notAList: [number] = [1]
// let notAList2: [number] = [1,2] // Type '[number, number]' is not assignable to type '[number]'.
// Source has 2 element(s) but target allows only 1.

let mixarray = [123, 'abc', true] //  (string | number | boolean)[]
mixarray[0] = true
mixarray[2] = 'xyz'
mixarray.push(123, '234', true)
// mixarray.push({}) // Argument of type '{}' is not assignable to parameter of type 'string | number | boolean'.

let aTuple: [userId: number, name: string, hobby: string] = [123, 'John', 'TypeScript']
//  aTuple = [ 'John', 123, 'TypeScript'] // Type 'string' is not assignable to type 'number'.
//  aTuple = [ 123, 'John', 'TypeScript', 'extra'] //  Source has 4 element(s) but target allows only 3.

// aTuple[2] = 123 // Error
aTuple[2] = 'test'

const tupl2: [number, string] = [123, 'Test']
// tupl2 = [] // error 
tupl2[0] = 234 // ok

/* Value as Literal Type Assertions */

const tupl3 = [123, 'Test'] as const
// tupl3[0] = 234 // Cannot assign to '0' because it is a read-only property

let name1 = 'john' as const
const name2 = 'john'
let name3 = 'john' as const // dont use value type, use value as Type

let name4: 'john' | 'alice' = 'alice'
name4 = 'john'
// name4 = 'placki' // Type '"placki"' is not assignable to type '"john" | "alice"'

// aTuple.slice() // (string | number)[]

/* Type Assertions */

let someValue: unknown = "this is a string";
someValue = 123
someValue = true

let strLength: number = (someValue as string).length // unsafe

let x1: string = ' 123'

class Person { }

let p1: Person = new Person()