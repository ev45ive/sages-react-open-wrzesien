export { }

const x: string[] = ['123', '123', '123']
const y: Array<string> = ['123', '123', '123']
const z: Array<string | number> = ['123', 123, '123']

const tictac: Array<Array<'X' | 'O'>> = [
  ['O', 'X', 'X'],
  ['O', 'X', 'O'],
  ['X', 'X', 'O'],
]

declare function parse<T>(params: string): T
declare function serialzie<T>(params: T): string

const res = parse<Array<string>>('["test"]')
const res1: Array<string> = parse('["test"]')
const res2 = parse('["test"]') as Array<string>
const res3: string = parse<string>('["test"]')

type Ref<T> = {
  current?: T
}
const ref: Ref<string> = {
  current: '123'
}
ref.current?.length

function createRef<T>(current: T): Ref<T> {
  return { current }
}
const ref3 = createRef('placki')
ref3.current?.length

/** A class definition with a generic parameter */
class Queue<T> {
  private data: T[] = [];
  push(item: T) { this.data.push(item); }
  pop(): T | undefined { return this.data.shift(); }
}

const q = new Queue<string | number>()
q.push('123')
q.pop() // string | number | undefined


/* Collection complex  */
interface GetItemsResponse<T extends { kind: string }, K extends T['kind'] = T['kind']> {
  kind: K,
  items: T[]
  total: number
}

interface Track {
  duration_ms: number;
  id: string;
  name: string;
  kind: 'track';
}
interface Show {
  duration_ms: number;
  id: string;
  name: string;
  kind: 'show';
}

const makeREs = <T extends { kind: string; }>(data: T[]): GetItemsResponse<T> => {
  return {
    items: data,
    kind: data[0].kind,
    total: data.length
  }
}

makeREs<Track | Show>([{ id: '123', kind: 'show', duration_ms: 123, name: 'test' }])

function takeFirst<T>(params: T[]): T {
  return params[0]
}

const arr1: string = takeFirst<string>(['123'])
const arr2 = takeFirst(['123'])
const arr3: string = takeFirst(['123', '123'])