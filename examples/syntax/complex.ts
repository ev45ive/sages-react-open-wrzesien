export { }

// library.ts
interface Item {
  type: "item"
  value: number
}

// app.ts - // Declaration merging
interface Item {
  extraData: string
}

type extendedItem = Item & { extra: string }

type primitiveAlias = string
// type primitiveAlias = string // Duplicate identifier 'primitiveAlias'.
type unionAlias = string | number | undefined
type item2 = { type: 'item', value: number }
type ItemUnderNewName = item2 // alias

function test(x2: unionAlias) {
  x2?.valueOf()
}


// const items: {
//   type: "item2";
//   value: number;
// } = {
//   type: 'item2' as const,
//   value: 32
// }


const value = {
  type: 'item2' as const,
  value: 32
}
type itemType = typeof value;

const anItem: Item = {
  type: 'item' as const,
  value: 32,
  extraData: '123'
}

takeItem(anItem)


function takeItem(item: ItemUnderNewName) {
  return item.value
}

// Structural vs Nominal

interface Point { x: number; y: number }
interface Vector { x: number; y: number; length: number }

let v: Vector = { x: 123, y: 123, length: 123 }
let p: Point = { x: 123, y: 123 }

// v = p  // Property 'length' is missing in type 'Point' but required in type 'Vector'.
p = v


